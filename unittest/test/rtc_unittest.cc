#include <iostream>
#include "../../rtc_i2c.h"
#include "gtest/gtest.h"

using ::testing::TestWithParam;
using ::testing::Range;

uint8_t rtc_reg[7];
uint8_t ref_reg[7];

// ****************************************************************************
//
// Winbond Write Unit Test
//
// ****************************************************************************
class RTCUnitTest : public ::testing::TestWithParam<int> {
	public:
		virtual void SetUp() {
			std::fill(rtc_reg, rtc_reg + 8, 0x00);
		}

		virtual void TearDown() {}

		void construct_ref_reg(uint8_t year,
				               uint8_t month,
				               uint8_t date,
				               uint8_t day,
				               uint8_t hour,
				               uint8_t min,
				               uint8_t sec) {

		    ref_reg[0] = dec2bcd(sec);
		    ref_reg[1] = dec2bcd(min);
		    ref_reg[2] = dec2bcd(hour);
		    ref_reg[3] = dec2bcd(day);
		    ref_reg[4] = dec2bcd(date);
		    ref_reg[5] = dec2bcd(month);
		    ref_reg[6] = dec2bcd(year); 
		}
};

TEST_P(RTCUnitTest, VerifyRTCSet) {
	set_rtc(18, 8, 2, 4, 16, 50, 0);
	construct_ref_reg(18, 8, 2, 4, 16, 50, 0);

	EXPECT_EQ(rtc_reg[GetParam()], ref_reg[GetParam()]);
}

TEST_P(RTCUnitTest, VerifyRTCRead) {
	set_rtc(18, 8, 2, 4, 16, 50, 0);
	read_rtc();

	EXPECT_EQ(rtc_reg[GetParam()], ref_reg[GetParam()]);
}

INSTANTIATE_TEST_CASE_P(SetupFlashAddressArray,
                        RTCUnitTest,
                        Range(0, 7));