#ifndef SPI_DRIVER_H_
#define SPI_DRIVER_H_

#include <stdint.h>
#include <stdbool.h>
	
#include "../inc/hw_memmap.h"
// #include "hw_ssi.h"
// #include "hw_types.h"
#include "../inc/hw_gpio.h"
// #include "ssi.h"
#include "../driverlib/gpio.h"
// #include "pin_map.h"
// #include "sysctl.h"
#include "../driverlib/interrupt.h"
// #include "tm4c123gh6pm.h"

// extern "C" {

//*****************************************************************************
//
// Function prototype
//
//*****************************************************************************
// void init_spi(void);
// uint8_t byte_transfer(uint8_t data);
// void SSI0IntHandler(void);

// } // extern "C"

#endif /* SPI_DRIVER_H_ */
