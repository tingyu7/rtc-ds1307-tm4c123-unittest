#ifndef WINBOND_H_
#define WINBOND_H_

#include "spi_driver.h"
#include <stdio.h>      /* printf */
#include <assert.h>     /* assert */
// #include "driverlib/uart.h"
// #include "utils/uartstdio.h"

extern "C" {

//*****************************************************************************
//
// WinBond flash commands
//
//*****************************************************************************
#define WB_WRITE_ENABLE       0x06
#define WB_WRITE_DISABLE      0x04
#define WB_CHIP_ERASE         0xc7
#define WB_READ_STATUS_REG_1  0x05
#define WB_READ_DATA          0x03
#define WB_PAGE_PROGRAM       0x02
#define WB_READ_ID            0x90
#define WB_BLOCK_ERASE_64     0xd8

#define WB_PAGE_SIZE          0x100  // 256  B
#define WB_BLOCK_SIZE_64      0x10000  //  64 kB

// For googletest only
// Virtual Winbond flash
extern uint8_t mem[1024];
extern uint8_t ref_mem[1024];

// Virtual reading location
extern uint8_t read_buf[1024];

// Virtual register space in RAM
//
// *All hex values below are offsets from base address*
// 0xFFC --- 0xFFF [1023]
//       .
//       .
//       .
// 0x004 --- 0x007 [1]
// 0x000 --- 0x003 [0]
extern uint32_t gpio_porta_base[1024]; // GPIO_PORTA_BASE

//*****************************************************************************
//
// Function prototype
//
//*****************************************************************************
// void init_uart(void);
// void print_page_bytes(uint8_t *page_buffer);
void chip_select(uint8_t val);
// void not_busy(void);
// void read_id(void);
void my_spi_read(uint32_t addr, uint32_t num_byte, uint8_t *dst);
void write_page(uint32_t addr, uint32_t num_byte, uint8_t *src);
void my_spi_write(uint32_t addr, uint32_t num_byte, uint8_t *src);
void block_erase_64KB(uint32_t addr);
void my_spi_erase(uint32_t addr, uint32_t num_byte);
// void chip_erase(void);

} // extern "C"

#endif /* WINBOND_H_ */
