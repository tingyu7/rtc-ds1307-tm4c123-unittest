# TM4C123GXL Launchpad + Tiny RTC DS1307

This project contains an interrupt driven I2C driver which is used to interface
with the Tiny RTC DS1307 module. This project is developed using CCS V7

## Source Files

### i2c_driver.h
Header file for i2c_driver.c

### i2c_driver.c
Generic I2C driver
TODO - Add error / ACK check

### rtc_i2c.h
Header file for rtc_i2c.c

### rtc_i2c.c
Main program containing RTC applications