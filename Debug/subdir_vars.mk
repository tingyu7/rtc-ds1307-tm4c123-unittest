################################################################################
# Automatically-generated file. Do not edit!
################################################################################

SHELL = cmd.exe

# Add inputs and outputs from these tool invocations to the build variables 
CMD_SRCS += \
../tm4c123gh6pm.cmd 

LIB_SRCS += \
C:/ti/TivaWare_C_Series-2.1.4.178/driverlib/ccs/Debug/driverlib.lib 

C_SRCS += \
../i2c_driver.c \
../rtc_i2c.c \
../tm4c123gh6pm_startup_ccs.c 

C_DEPS += \
./i2c_driver.d \
./rtc_i2c.d \
./tm4c123gh6pm_startup_ccs.d 

OBJS += \
./i2c_driver.obj \
./rtc_i2c.obj \
./tm4c123gh6pm_startup_ccs.obj 

OBJS__QUOTED += \
"i2c_driver.obj" \
"rtc_i2c.obj" \
"tm4c123gh6pm_startup_ccs.obj" 

C_DEPS__QUOTED += \
"i2c_driver.d" \
"rtc_i2c.d" \
"tm4c123gh6pm_startup_ccs.d" 

C_SRCS__QUOTED += \
"../i2c_driver.c" \
"../rtc_i2c.c" \
"../tm4c123gh6pm_startup_ccs.c" 


